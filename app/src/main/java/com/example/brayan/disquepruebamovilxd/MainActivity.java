package com.example.brayan.disquepruebamovilxd;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    Button revisar;
    EditText numero;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        numero = (EditText)findViewById(R.id.TXTNumero);
        revisar = (Button)findViewById(R.id.BTNRevisar);

        revisar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (numero.getText().toString().isEmpty()){
                    numero.setError(getString(R.string.Hint2));
                }else{

                    String NumeroSemana = numero.getText().toString();
                    Intent intent = new Intent(MainActivity.this, Main2Activity.class);
                    intent.putExtra("Semana", NumeroSemana);
                    numero.setText("");
                    startActivity(intent);
                    Log.e("Mensaje", NumeroSemana+ " Bien!");
                }
            }
        });

    }
}
