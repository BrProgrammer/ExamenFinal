package com.example.brayan.disquepruebamovilxd;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;

import static java.util.Calendar.WEEK_OF_YEAR;

public class Main2Activity extends AppCompatActivity {

    TextView Bien, Mal;
    Calendar calendar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Bien = (TextView)findViewById(R.id.LBLBien);
        Mal = (TextView)findViewById(R.id.LBLMal);
        String dato = getIntent().getStringExtra("Semana");
        int Semana= Integer.parseInt(dato);
        calendar = Calendar.getInstance();
        if (Semana==calendar.get(WEEK_OF_YEAR)){
            Bien.setText("HAS ACERTADO");
        }else {
            Mal.setText("INTENTA NUEVAMENTE");

        }


    }
}
